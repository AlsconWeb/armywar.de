<?php
/*
 Template Name: Cards
 */
?>
<?php get_header(); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div id="primary" class="site-content">
                    <div id="content" role="main">
                        <?php while ( have_posts() ) : the_post(); ?>

                            <article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <header class="entry-header">
                                    <h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
                                </header>

                                <div class="entry-content" itemprop="text">

                                    <div class="row">
                                        <div class="col-12">
                                            <?php
                                            global $post;
                                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                            if(1 == $paged) :
                                                $topText = get_post_custom_values($key = 'top_text_block'); ?>
                                                <?php if($topText[0]) : ?>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="top-bar">
                                                            <?php echo do_shortcode($topText[0]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php endif; ?>

                                            <!--Posts list start-->

                                            <?php
                                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                            $catForQuery = detect_cat_title_1();
                                            $i = 1;
                                            $gs1_args = array(
                                                'category_name' => $catForQuery,
                                                'post_status' => 'publish',
                                                'posts_per_page' => '21',
                                                'paged' => $paged,
                                                'order' => 'ASC',
                                                'orderby' => 'ID'
                                            )
                                            ?>
                                            <?php $gs1 = new WP_Query( $gs1_args ); ?>
                                            <div class="row">
                                                <div class="col-12 d-flex align-items-center justify-content-center">
                                                    <?php if($GLOBALS['wp_query']->max_num_pages > 1) : ?><span><?php _e('Seite', 'iconic-one-child')?></span><?php endif; ?>
                                                    <?php $GLOBALS['wp_query']->max_num_pages = $gs1->max_num_pages; the_posts_pagination( array( 'mid_size' => 1, 'end_size'=>1) );  ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="masonry">
                                                        <?php if ( $gs1->have_posts() ) : ?>
                                                            <?php while ( $gs1->have_posts() ) : $gs1->the_post(); ?>
                                                                <div class="masonry__brick">
                                                                    <div class="masonry__brick-outer">
                                                                        <div class="masonry__brick-inner">
                                                                            <?php $authorName1 =  get_field( 'quote_author' ); ?>
                                                                            <?php if( has_post_thumbnail() ) : ?>
                                                                                <div class="masonry__brick-top">
                                                                                    <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php _e( 'Zitat anzeigen', 'iconic-one-child' ); ?>" rel="bookmark">
                                                                                        <div class="masonry__brick-top-overlayer">
                                                                                            <?php
                                                                                            the_post_thumbnail( 'full', array(

                                                                                                'alt'   => the_title_attribute( 'echo=0' ),
                                                                                            ) );
                                                                                            ?>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                            <div class="masonry__brick-bottom">
                                                                                <h3 class="text">
                                                                                    <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php _e( 'Zitat anzeigen', 'iconic-one-child' ); ?>" rel="bookmark">
                                                                                        <?php echo get_the_content(); ?>
                                                                                    </a>
                                                                                </h3>
                                                                                <?php
                                                                                $authorName1 =  get_field( 'quote_author' );
                                                                                ?>
                                                                                <p class="author">
                                                                                    <?php echo $authorName1; ?>
                                                                                </p>
                                                                            </div>
                                                                            <div class="share-btn-block">
                                                                                <span class="screen-reader-text"><?php _e( 'Teilen:', 'iconic-one-child' ); ?></span>
                                                                                <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php if($i % 7 == 0) :  ?>
                                                                    <div class="desc-text">
                                                                        <?php
                                                                        $descPostText =  get_post_custom_values($key = 'descriptive_post_text' );
                                                                        if($descPostText[0]) {
                                                                          echo do_shortcode($descPostText[0]);
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                <?php endif; $i++; ?>
                                                            <?php endwhile; ?>
                                                            <?php wp_reset_postdata(); ?>
                                                        <?php else : ?>
                                                            <p><?php _e( 'Die Zitate wurden nicht gefunden', 'iconic-one-child' ); ?></p>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row d-flex">
                                                <div class="col-12 d-flex align-items-center justify-content-center">
                                                    <?php if($GLOBALS['wp_query']->max_num_pages > 1) : ?><span class="screen-reader-text"><?php _e('Seite', 'iconic-one-child')?></span><?php endif; ?>
                                                    <?php $GLOBALS['wp_query']->max_num_pages = $gs1->max_num_pages; the_posts_pagination( array( 'mid_size' => 1, 'end_size'=>1 ) );  ?>
                                                </div>
                                                <div class="col-12 d-flex align-self-stretch align-items-center justify-content-flex-end">
                                                    <?php
                                                    if(function_exists("kk_star_ratings")) :
                                                        global $post;
                                                        $pid = $post->ID;
                                                        echo kk_star_ratings($pid);
                                                    endif;
                                                    ?>
                                                </div>
                                            </div>
                                            <!--Posts list end-->
                                        </div>
                                    </div>

                                </div><!-- .entry-content -->

                            </article><!-- #post -->

                            <?php comments_template( '', false ); ?>
                        <?php endwhile; // end of the loop. ?>

                    </div><!-- #content -->
                </div><!-- #primary -->
            </div>
            <div class="col-12 col-md-4 d-none d-md-block">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
    <?php
    if(1 == $paged) :
        $key_name = get_post_custom_values($key = 'bottom_text_block'); ?>
        <?php if($key_name[0]) : ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bottom-bar">
                        <?php echo do_shortcode($key_name[0]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; /*end if $key_name*/?>
    <?php endif; /*end if 1st page*/?>
<?php get_footer(); ?>