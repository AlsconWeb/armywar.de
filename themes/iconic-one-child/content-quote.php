<?php
/*
 * Content display template, used for single quote posts.
 * File Last updated: Iconic One Child 1.0.0
 */
?>

<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="single-layout-1">
        <header class="entry-header">

            <h1 class="entry-title" itemprop="headline">
                <?php show_title_by_cat_1(); ?>
            </h1>

        </header><!-- .entry-header -->

        <div class="row">
            <div class="col-12">
                <?php
                the_post_navigation( array(
                    'next_text' => '<span class="meta-nav" aria-hidden="true">&gt;</span> ' .
                        '<span class="screen-reader-text">'.__('Nächste Seite', 'iconic-one-child').'</span> ' .
                        '<span class="post-title"></span>',
                    'prev_text' => '<span class="meta-nav" aria-hidden="true">&lt;</span> ' .
                        '<span class="screen-reader-text">'.__('vorherige Seite', 'iconic-one-child').'</span> ' .
                        '<span class="post-title"></span>',
                    'in_same_term' => true,
                ) );
                ?>
            </div><!-- end navigation -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="entry-content" itemprop="text">

                    <div class="masonry__brick-outer">
                        <div class="masonry__brick-inner">
                            <?php if( has_post_thumbnail() ) : ?>
                                <div class="masonry__brick-top">
                                    <div class="masonry__brick-top-overlayer">
                                        <?php
                                        the_post_thumbnail( 'full', array(
                                            'alt'   => the_title_attribute( 'echo=0' ),
                                        ) );
                                        ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                                <div class="masonry__brick-center" style="display: none">

                                    <!--preview posts links block start-->
                                    <?php
                                    $currentID = get_the_ID();
                                    $category = get_the_category();
                                    $firstCategory = $category[0]->cat_name;
                                    $gs2_args = array(
                                        'category_name' => $firstCategory,
                                        'post_status' => 'publish',
                                        'posts_per_page' => '-1'
                                    )
                                    ?>
                                    <?php $gs2 = new WP_Query( $gs2_args ); ?>
                                    <?php if ( $gs2->have_posts() ) : ?>
                                    <?php while ( $gs2->have_posts() ) : $gs2->the_post(); ?>
                                    <?php
                                    $post_id = get_the_ID();
                                    if ( $post_id == $currentID ) {
                                    ?>
                                    <div class="masonry__brick-center-item current">
                                    <?php } else { ?>
                                    <div class="masonry__brick-center-item">
                                    <?php } ?>
                                        <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php _e( 'view quote', 'iconic-one-child' ); ?>" rel="bookmark">
                                            <?php if( has_post_thumbnail() ) { ?>
                                                <div class="masonry__brick-center-item--inner">
                                                    <?php
                                                    the_post_thumbnail( 'full', array(

                                                        'alt'   => the_title_attribute( 'echo=0' ),
                                                    ) );
                                                    ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="masonry__brick-center-item--inner"></div>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                                    <?php endif; ?>
                                    <!--preview posts links block end-->
                                </div>
                                <div class="masonry__brick-bottom">
                                    <h3 class="text">
                                        <?php the_content(); ?>
                                    </h3>
                                    <p class="author">
                                        <?php
                                        $authorName1 =  get_field( 'quote_author' );
                                        echo $authorName1;
                                        ?>
                                    </p>
                                </div>
                                <div class="share-btn-block">
                                    <span class="screen-reader-text"><?php _e( 'Teilen:', 'iconic-one-child' ); ?></span>
                                    <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
                                </div>
                            </div>
                        </div> <!--.masonry__brick-inner-->
                    </div> <!--.masonty-brick__outer-->
                </div> <!-- .entry-content -->
            </div> <!--#col-->
        </div> <!--#row-->
    </div>
</article><!-- #post-## -->
