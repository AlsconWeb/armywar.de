<?php
add_action('wp_enqueue_scripts', 'iconic_one_child_enqueue_styles');
function iconic_one_child_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_script('script-custom', get_stylesheet_directory_uri() . '/assets/js/custom.js', array(), '1.0.0', true);
}

add_action('wp', 'add_slick_script_on_single');
function add_slick_script_on_single(){
    if( is_single() )
        add_action( 'wp_enqueue_scripts', 'slick_scripts' );
}
function slick_scripts() {
    wp_enqueue_style( 'style-slick', get_stylesheet_directory_uri() . '/assets/slick/slick.css', array(), false, 'all' );
    wp_enqueue_style( 'style-slick-theme', get_stylesheet_directory_uri() . '/assets/slick/slick-theme.css', array(), false, 'all' );
    wp_enqueue_script( 'script-slick', get_stylesheet_directory_uri() . '/assets/slick/slick.min.js', array(), false, true );
}

add_theme_support( 'post-formats', array( 'quote' ) );

/*
* Creating a function to create our CPT
*/

/*
* Authors post type
*/
function authors_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Authors', 'Post Type General Name', 'iconic-one-child'),
        'singular_name' => _x('Author', 'Post Type Singular Name', 'iconic-one-child'),
        'menu_name' => __('Authors', 'iconic-one-child'),
        'parent_item_colon' => __('Parent Author', 'iconic-one-child'),
        'all_items' => __('All Authors', 'iconic-one-child'),
        'view_item' => __('View Author', 'iconic-one-child'),
        'add_new_item' => __('Add New Author', 'iconic-one-child'),
        'add_new' => __('Add New', 'iconic-one-child'),
        'edit_item' => __('Edit Author', 'iconic-one-child'),
        'update_item' => __('Update Author', 'iconic-one-child'),
        'search_items' => __('Search for Author', 'iconic-one-child'),
        'not_found' => __('Not Found', 'iconic-one-child'),
        'not_found_in_trash' => __('Not found in Trash', 'iconic-one-child'),
    );

// Set other options for Custom Post Type

    $args = array(
        'description' => __('Author description', 'iconic-one-child'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('sections'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'authors')
    );

    // Registering your Custom Post Type
    register_post_type('authors', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'authors_post_type', 0);

/*detect category by page title*/
if ( ! function_exists( 'detect_cat_title_1' ) ) :
    function detect_cat_title_1() {
        global $post;
        $pageSlug = $post->post_name;
        return $pageSlug;
    }
endif;

/*show single page title by category name*/
if ( ! function_exists( 'show_title_by_cat_1' ) ) :
    function show_title_by_cat_1() {
        $cat = get_the_category();
        $cattitle = '';
        foreach ($cat as $cati) {
            if ($cati->cat_name == 'Geburtstagswünsche') {
                $cattitle .= 'Geburtstagswünsche ';
            } else if ($cati->cat_name == 'Geburtstagsgedichte') {
                $cattitle .= 'Geburtstagsgedichte ';
            } else if ($cati->cat_name == 'Geburtstagsgrüße') {
                $cattitle .= 'Geburtstagsgrüße ';
            } else if ($cati->cat_name == 'Geburtstagssprüche') {
                $cattitle .= 'Geburtstagssprüche ';
            } else {
                $cattitle .= $cati->cat_name . ' ';
            }
        }
        $catTitlesArr = explode(' ', trim($cattitle));
        /*$catTitles = implode(' | ', $catTitlesArr);*/
        $catTitles = implode(' ', $catTitlesArr);
        echo $catTitles;
    }
endif;

/*Configure robots meta tag*/
if ( ! function_exists( 'orderToRobots' ) ) :
    function orderToRobots() {
        global $post;
        if( is_page() || is_paged() ) {
            echo( '<meta name="robots" content="index,follow" />' );
        } else {
            echo( '<meta name="robots" content="noindex,follow" />' );
        }
        return false;
    }
endif;
add_action( 'wp_head', 'orderToRobots' );

/*Month shortcode*/
function currentM_shortcode() {
    $res = date_i18n('F', false, false);
    return $res;
}
add_shortcode('currentM', 'currentM_shortcode');

/*Year shortcode*/
function currentY_shortcode() {
    $res = date('Y');
    return $res;
}
add_shortcode('currentY', 'currentY_shortcode');

function my_shortcode_title( $title ){
    return do_shortcode( $title );
}
add_filter( 'single_post_title', 'my_shortcode_title' );
add_filter( 'the_title', 'my_shortcode_title' );


function gb_acf_format_value($value, $post_id, $field){
    $value = do_shortcode($value);
    return $value;
}

add_filter('acf/format_value', 'gb_acf_format_value', 11, 3);

//translate slug in pagination
function pilsak_rewrite_rules() {
    global $wp_rewrite;
    $wp_rewrite->pagination_base    = 'seite';
}
add_action('init', 'pilsak_rewrite_rules');


//translate title in pagination
function pilsak_filter_wp_title( $title, $separator ) {
    // Globalize $page
    global $page;

    // Determine if current post is paginated
    // and if we're on a page other than Page 1
    if ( $page >= 2 ) {
        // Append $separator Page #
        $title .= ' ' . $separator . ' ' . 'Seite ' . $page;
    }
    // Return filtered $title
    return $title;
}
add_filter( 'wp_title', 'pilsak_filter_wp_title', 10, 2 );


?>