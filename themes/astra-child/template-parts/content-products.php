<?php
$productID = get_sub_field('product_section_id');
// args
$products_args = array(
    'post_type'		    => 'products',
    'posts_per_page'	=> 1,
    'meta_key'		    => 'blog_add_content_section_prod_id',
    'meta_value'	    => $productID
);
// query
$products = new WP_Query( $products_args );
?>
<?php if( $products->have_posts() ): ?>
    <?php while( $products->have_posts() ) : $products->the_post(); ?>
        <!--Product section start-->
        <div class="container px-0 my-3">
            <div class="add-content__product">
                <div class="row align-items-center">
                    <div class="col-12 col-md-3">
                        <div class="add-content__product-img text-center">
                            <a href="<?php echo get_field('blog_add_content_section_prod_url'); ?>" target="_blank" title="<?php echo strip_tags( get_the_content() );?>">
                                <img src="<?php echo get_field('blog_add_content_section_prod_img'); ?>" alt="product image" >
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="add-content__product-content">
                            <div class="add-content__product-content-desc">
                                <a href="<?php echo get_field('blog_add_content_section_prod_url'); ?>" target="_blank" title="<?php echo strip_tags( get_the_content() );?>">
                                    <?php echo the_excerpt_max_charlength(100); ?>
                                </a>
                            </div>
                            <?php if (have_rows('blog_add_content_section_prod_note')) : ?>
                                <?php while (have_rows('blog_add_content_section_prod_note')) : the_row();
                                    $noteText = get_sub_field('blog_add_content_section_prod_note_t');
                                    $noteNum = get_sub_field('blog_add_content_section_prod_note_n');
                                    ?>
                                    <?php if ($noteText || $noteNum) : ?>
                                        <div class="add-content__product-content-note">
                                            <span><?php _e('Gesamtnote', 'astra-child'); ?>:&nbsp;</span>
                                            <?php if ($noteText) : ?>
                                                <span class="add-content__product-content-note-text"><?php echo $noteText; ?></span>
                                            <?php endif; ?>
                                            <?php if ($noteNum) : ?>
                                                <span class="add-content__product-content-note-num"><?php echo '&nbsp;(' . $noteNum . ')'; ?></span>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <a href="<?php echo get_field('blog_add_content_section_prod_url'); ?>" role="button" target="_blank" class="add-content__product-btn">
                            <span class="icon-cart"></span>
                            <?php $marketPlace = get_field('blog_add_content_section_prod_marketplace'); ?>
                            <?php if ($marketPlace) : ?>
                                <span><?php echo $marketPlace; ?></span>
                            <?php endif; ?>
                            <?php if (have_rows('blog_add_content_section_prod_price')) : ?>
                                <?php while (have_rows('blog_add_content_section_prod_price')) : the_row();
                                    $priceNum = get_sub_field('blog_add_content_section_prod_price_amount');
                                    $priceCurr = get_sub_field('blog_add_content_section_prod_price_currency');
                                    ?>
                                    <?php if ($priceCurr) : ?>
                                        <span><?php echo $priceCurr; ?></span>
                                    <?php endif; ?>
                                    <?php if ($priceNum) : ?>
                                        <span><?php echo $priceNum; ?></span>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <!--Product section end-->
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'The product has not been found', 'astra-child' ); ?></p>
<?php endif; ?>
