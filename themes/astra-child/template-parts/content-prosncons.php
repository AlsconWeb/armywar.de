<?php if(have_rows('pros_cons_section_group')) : ?>
    <?php while(have_rows('pros_cons_section_group')) : the_row(); ?>
        <!--Pros and cons section start-->
        <div class="container px-0 my-3">
            <div class="add-content__prosncons">
                <div class="row">
                    <?php if(have_rows('pros_cons_section_pros')) : ?>
                        <?php while(have_rows('pros_cons_section_pros')) : the_row(); ?>
                            <div class="col-12 col-md-6">
                                <div class="add-content__prosncons-col pros">
                                    <?php $colTitleP = get_sub_field('pros_cons_section_pros_h'); ?>
                                    <?php if($colTitleP) : ?>
                                        <h3 class="add-content__prosncons-col-h"><?php echo $colTitleP; ?></h3>
                                    <?php endif; ?>
                                    <?php if(have_rows('pros_cons_section_pros_items')) : ?>
                                        <ul class="add-content__prosncons-col-list">
                                            <?php while(have_rows('pros_cons_section_pros_items')) : the_row();
                                            $itemP = get_sub_field('pros_cons_section_pros_item_t');
                                            ?>
                                                <li><span><?php echo $itemP; ?></span></li>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if(have_rows('pros_cons_section_cons')) : ?>
                        <?php while(have_rows('pros_cons_section_cons')) : the_row(); ?>
                            <div class="col-12 col-md-6">
                                <div class="add-content__prosncons-col cons">
                                    <?php $colTitleC = get_sub_field('pros_cons_section_cons_h'); ?>
                                    <?php if($colTitleC) : ?>
                                        <h3 class="add-content__prosncons-col-h"><?php echo $colTitleC; ?></h3>
                                    <?php endif; ?>
                                    <?php if(have_rows('pros_cons_section_cons_items')) : ?>
                                        <ul class="add-content__prosncons-col-list">
                                            <?php while(have_rows('pros_cons_section_cons_items')) : the_row();
                                                $itemC = get_sub_field('pros_cons_section_cons_item_t');
                                                ?>
                                                <li><span><?php echo $itemC; ?></span></li>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!--Pros and cons section end-->
    <?php endwhile; ?>
<?php endif; ?>
