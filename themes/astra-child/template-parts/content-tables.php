<?php
$tableID = get_sub_field('table_section_id');
// args
$tables_args = array(
    'post_type'		    => 'tables',
    'posts_per_page'	=> 1,
    'meta_key'		    => 'blog_add_content_section_t_id',
    'meta_value'	    => $tableID
);
// query
$tables = new WP_Query( $tables_args );
?>
<?php if( $tables->have_posts() ): ?>
    <?php while( $tables->have_posts() ) : $tables->the_post(); ?>
        <div class="container add-content__table px-0">
            <?php if (have_rows('blog_add_content_section_th')) : ?>
                <?php while (have_rows('blog_add_content_section_th')) : the_row(); ?>
                    <?php
                    $thCol2 = get_sub_field('blog_add_content_section_th_td_2');
                    $thCol3 = get_sub_field('blog_add_content_section_th_td_3');
                    $thCol4 = get_sub_field('blog_add_content_section_th_td_4');
                    ?>
                    <!--Table heading-->
                    <div class="add-content__table-th">
                        <div class="row">
                            <div class="col-sm-7 col-md-4 d-none d-sm-block">
                            <span>
                                <?php if (have_rows('blog_add_content_section_th_td_1')) : ?>
                                    <?php while (have_rows('blog_add_content_section_th_td_1')) : the_row(); ?>
                                        <?php
                                        $thCol1Title = get_sub_field('blog_add_content_section_th_td_1_1');
                                        $thCol1Text = get_sub_field('blog_add_content_section_th_td_1_2');
                                        ?>
                                        <?php if ($thCol1Title) : ?>
                                            <strong><?php echo $thCol1Title; ?>: </strong>
                                        <?php endif; ?>
                                        <?php if ($thCol1Text) :
                                            echo $thCol1Text;
                                        endif; ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </span>
                            </div>
                            <div class="col-12 col-sm-5 col-md-6 col-lg-5">
                                <div class="row">
                                    <div class="col-3 col-sm-6 col-md-3 text-center">
                                        <span><?php echo ($thCol2) ? $thCol2 : ''; ?></span>
                                    </div>
                                    <div class="col-6 col-md-6 d-sm-none d-md-block">
                                        <div class="row">
                                            <div class="col-9 text-right"><span><?php echo ($thCol3) ? $thCol3 : ''; ?></span>
                                            </div>
                                            <div class="col-3 d-none d-sm-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-3 col-sm-6 col-md-3 text-center">
                                        <span><?php echo ($thCol4) ? $thCol4 : ''; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2 col-md-2"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4 d-sm-none">
                            <?php if (have_rows('blog_add_content_section_th_td_1')) : ?>
                                <?php while (have_rows('blog_add_content_section_th_td_1')) : the_row(); ?>
                                    <?php
                                    $thCol1Title = get_sub_field('blog_add_content_section_th_td_1_1');
                                    $thCol1Text = get_sub_field('blog_add_content_section_th_td_1_2');
                                    ?>
                                    <div class="add-content__table-th-info">
                                        <button type="button" class="info-btn" id="infoHinweis">
                                            <span><?php echo ($thCol1Title) ? $thCol1Title : 'Hinweis'; ?>:&nbsp;</span><span
                                                class="info-icon">i</span></button>
                                        <div class="info-text">
                                            <div class="info-text--head">
                                                <span><?php echo ($thCol1Title) ? $thCol1Title : 'Hinweis'; ?></span></div>
                                            <div class="info-text--body">
                                                <span><?php echo ($thCol1Text) ? $thCol1Text : ''; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <!--Table body start-->
            <?php if (have_rows('blog_add_content_section_tbody_tr')) : ?>
                <?php $rowCounter = 1; ?>
                <?php while (have_rows('blog_add_content_section_tbody_tr')) : the_row(); ?>
                    <!--Table row-->
                    <div class="add-content__table-td">
                        <div class="row">
                            <!--block 1-->
                            <div class="col-12 col-sm-7 col-md-4">
                                <div class="row">
                                    <!--column 1-->
                                    <div class="col-4 col-sm-6 col-md-5">
                                        <a href="<?php echo get_sub_field('blog_add_content_section_tbody_tr_td_url'); ?>" rel="nofollow"
                                           target="_blank" class="d-block text-center">
                                            <?php if (have_rows('blog_add_content_section_tbody_tr_td2')) : ?>
                                                <?php while (have_rows('blog_add_content_section_tbody_tr_td2')) : the_row();
                                                $col2Main = get_sub_field('blog_add_content_section_tbody_tr_td2_1');
                                                $col2Sec = get_sub_field('blog_add_content_section_tbody_tr_td2_2');
                                                endwhile; ?>
                                            <?php endif; ?>
                                            <img src="<?php echo get_sub_field('blog_add_content_section_tbody_tr_td1'); ?>"
                                                 alt="<?php echo $col2Main . '-' . $col2Sec; ?>">
                                        </a>
                                    </div>
                                    <!--column 2-->
                                    <div class="col-8 col-sm-6 col-md-7">
                                        <span style="display: inline-block;margin-bottom: 5px;"><strong><?php echo $col2Main; ?></strong></span><br>
                                        <span style="display: inline-block;font-size: 12px;margin-bottom: 5px;"><strong><?php echo $col2Sec; ?></strong></span><br>
                                        <?php if (get_sub_field('blog_add_content_section_tbody_tr_td_is_add_rating')) : ?>
                                            <!--Rating box start-->
                                            <div class="add-content__table-td-rating-box">
                                                <?php if (have_rows('blog_add_content_section_tbody_tr_td_rating_total')) : ?>
                                                    <?php while (have_rows('blog_add_content_section_tbody_tr_td_rating_total')) : the_row(); ?>
                                                        <div class="add-content__table-td-rating">
                                                            <?php
                                                            $getRating = get_sub_field('blog_add_content_section_tbody_tr_td_rating_total_num');
                                                            $getRatingT = get_sub_field('blog_add_content_section_tbody_tr_td_rating_transparenz_number');
                                                            $getRatingS = get_sub_field('blog_add_content_section_tbody_tr_td_rating_service_number');
                                                            $getRatingW = get_sub_field('blog_add_content_section_tbody_tr_td_rating_weiterempfehlung_number');
                                                            $getRatingOB = get_sub_field('blog_add_content_section_tbody_tr_td_rating_online_banking_number');
                                                            $numStars = 5;
                                                            $ratingWidth = (($getRating * 100) / $numStars) + 2;
                                                            ?>
                                                            <div class="add-content__table-td-rating-total">
                                                                <div class="add-content__table-td-rating-bar"
                                                                     style="width:<?php echo $ratingWidth; ?>%;"></div>
                                                                <div class="add-content__table-td-rating-stars"><img
                                                                        src="<?php echo get_stylesheet_directory_uri(); ?>/images/stars-cover.svg"
                                                                        alt="rating stars"></div>
                                                            </div>
                                                            <?php if (($getRatingT > 0) || ($getRatingS > 0) || ($getRatingW > 0) || ($getRatingOB > 0)) : ?>
                                                                <div class="add-content__table-td-rating-details">
                                                                    <p>Die Gesamtbewertung setzt sich aus den folgenden
                                                                        Bewertungen zusammen:</p>
                                                                    <?php if ($getRatingT > 0) : ?>
                                                                        <span>Transparenz:&nbsp;<?php echo $getRatingT; ?>&nbsp;von&nbsp;<?php echo $numStars; ?>&nbsp;Sternen</span>
                                                                        <div class="add-content__table-td-rating-total">
                                                                            <div class="add-content__table-td-rating-bar"
                                                                                 style="width:<?php echo (($getRatingT * 100) / $numStars) + 2; ?>%;"></div>
                                                                            <div class="add-content__table-td-rating-stars"><img
                                                                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/stars-cover.svg"
                                                                                    alt="rating stars"></div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <?php if ($getRatingS > 0) : ?>
                                                                        <span>Service:&nbsp;<?php echo $getRatingS; ?>&nbsp;von&nbsp;<?php echo $numStars; ?>&nbsp;Sternen</span>
                                                                        <div class="add-content__table-td-rating-total">
                                                                            <div class="add-content__table-td-rating-bar"
                                                                                 style="width:<?php echo (($getRatingS * 100) / $numStars) + 2; ?>%;"></div>
                                                                            <div class="add-content__table-td-rating-stars"><img
                                                                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/stars-cover.svg"
                                                                                    alt="rating stars"></div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <?php if ($getRatingW > 0) : ?>
                                                                        <span>Weiterempfehlung:&nbsp;<?php echo $getRatingW; ?>&nbsp;von&nbsp;<?php echo $numStars; ?>&nbsp;Sternen</span>
                                                                        <div class="add-content__table-td-rating-total">
                                                                            <div class="add-content__table-td-rating-bar"
                                                                                 style="width:<?php echo (($getRatingW * 100) / $numStars) + 2; ?>%;"></div>
                                                                            <div class="add-content__table-td-rating-stars"><img
                                                                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/stars-cover.svg"
                                                                                    alt="rating stars"></div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <?php if ($getRatingOB > 0) : ?>
                                                                        <span>Online-Banking:&nbsp;<?php echo $getRatingOB; ?>&nbsp;von&nbsp;<?php echo $numStars; ?>&nbsp;Sternen</span>
                                                                        <div class="add-content__table-td-rating-total">
                                                                            <div class="add-content__table-td-rating-bar"
                                                                                 style="width:<?php echo (($getRatingOB * 100) / $numStars) + 2; ?>%;"></div>
                                                                            <div class="add-content__table-td-rating-stars"><img
                                                                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/stars-cover.svg"
                                                                                    alt="rating stars"></div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span>(<?php echo get_sub_field('blog_add_content_section_tbody_tr_td_rating_total_bewertungen'); ?>&nbsp;Bewertungen)</span>
                                                        <input type="hidden" value="<?php echo $getRating; ?>">
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                            <!--Rating box end-->
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <!--block 2-->
                            <div class="col-12 d-md-none add-content__table-td-bt"></div>
                            <!--block 3-->
                            <div class="col-12 col-sm-5 col-md-6 col-lg-5">
                                <div class="row">
                                    <!--column 3-->
                                    <div class="col-3 col-sm-6 col-md-3 text-center">
                                        <?php if (have_rows('blog_add_content_section_tbody_tr_td3')) : ?>
                                            <?php while (have_rows('blog_add_content_section_tbody_tr_td3')) : the_row(); ?>
                                                <span class="d-inline-block mb-1 color-green"><?php echo get_sub_field('blog_add_content_section_tbody_tr_td3_1'); ?></span>
                                                <br>
                                                <span class="d-inline-block mb-1"><?php echo get_sub_field('blog_add_content_section_tbody_tr_td3_2'); ?></span>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!--column 4-->
                                    <div class="col-6 col-md-6 d-sm-none d-md-block text-center">
                                        <?php if (have_rows('blog_add_content_section_tbody_tr_td4')) : ?>
                                            <?php while (have_rows('blog_add_content_section_tbody_tr_td4')) : the_row(); ?>
                                                <div class="row">
                                                    <!--column 4 left side-->
                                                    <div class="col-12 col-sm-9 text-right">
                                                        <?php if (get_sub_field('blog_add_content_section_tbody_tr_td4_has_ib')) : ?>
                                                            <a href="javascript:void(0)" role="button" class="info-btn-automaten">
                                                                <span><?php echo get_sub_field('blog_add_content_section_tbody_tr_td4_1'); ?></span>
                                                                <span>&nbsp;</span><span class="info-icon">i</span></span>
                                                            </a>
                                                            <div class="automaten-block">
                                                                <div class="automaten-block__head">
                                                                    <span><?php echo get_sub_field('blog_add_content_section_tbody_tr_td4_ib_title'); ?></span>
                                                                </div>
                                                                <div class="automaten-block__body">
                                                                    <?php if (have_rows('blog_add_content_section_tbody_tr_td4_ib_rows')) : ?>
                                                                        <?php while (have_rows('blog_add_content_section_tbody_tr_td4_ib_rows')) : the_row();
                                                                            $infoBoxLCol = get_sub_field('blog_add_content_section_tbody_tr_td4_ib_row_l_col');
                                                                            $infoBoxRCol = get_sub_field('blog_add_content_section_tbody_tr_td4_ib_row_r_col');
                                                                            ?>
                                                                            <div class="automaten-block__body-section container">
                                                                                <div class="row">
                                                                                    <div class="<?php echo ($infoBoxRCol) ? ' col-10 ' : ' col-12 '; ?>">
                                                                                        <p><?php echo $infoBoxLCol; ?></p>
                                                                                    </div>
                                                                                    <?php if ($infoBoxRCol) : ?>
                                                                                        <div class="col-2 text-right">
                                                                                            <img src="<?php echo $infoBoxRCol; ?>" alt="info box section image">
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>
                                                                        <?php endwhile; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <span><?php echo get_sub_field('blog_add_content_section_tbody_tr_td4_1'); ?></span>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <!--column 4 right side-->
                                                    <div class="col-12 col-sm-3 d-none d-sm-block text-center payment-block">
                                                        <?php if (have_rows('blog_add_content_section_tbody_tr_td4_2')) : ?>
                                                            <?php while (have_rows('blog_add_content_section_tbody_tr_td4_2')) : the_row(); ?>
                                                                <div>
                                                                    <img src="<?php echo get_sub_field('blog_add_content_section_tbody_tr_td4_2_img'); ?>" alt="column 4 image">
                                                                </div>
                                                            <?php endwhile; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!--column 5-->
                                    <div class="col-3 col-sm-6 col-md-3 text-center">
                                        <?php if (have_rows('blog_add_content_section_tbody_tr_td5')) : ?>
                                            <?php while (have_rows('blog_add_content_section_tbody_tr_td5')) : the_row();
                                            $col5Num = get_sub_field('blog_add_content_section_tbody_tr_td5_1');
                                            $col5Curr = get_sub_field('blog_add_content_section_tbody_tr_td5_2');
                                            $col5Text = get_sub_field('blog_add_content_section_tbody_tr_td5_3');
                                            ?>
                                                <?php if ($col5Num || $col5Curr) : ?>
                                                    <span class="d-inline-block mb-1 color-green">
                                                        <?php if ($col5Num ) : ?>
                                                            <span><?php echo ($col5Num > 0) ? '+' . number_format($col5Num, '2', ',', '') : number_format($col5Num, '2', ',', ''); ?></span>
                                                        <?php endif; ?>
                                                        <?php if ($col5Curr ) : ?>
                                                            <span><?php echo $col5Curr; ?></span>
                                                        <?php endif; ?>
                                                    </span><br>
                                                <?php endif; ?>
                                                <?php if ($col5Text) : ?>
                                                    <span class="d-inline-block mb-1" style="font-size: 12px;"><?php echo $col5Text; ?></span>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <!--block 4-->
                            <div class="col-12 d-md-none add-content__table-td-bt"></div>
                            <!--block 5-->
                            <!--column 6-->
                            <div class="col-3 col-sm-4 col-md-2 col-lg-3 order-5 order-sm-4 text-right">
                                <a href="<?php echo get_sub_field('blog_add_content_section_tbody_tr_td_url'); ?>" rel="nofollow" target="_blank"
                                   class="add-content__table-td-btn">
                                    <span><?php echo (get_sub_field('blog_add_content_section_tbody_tr_td_url_btn_text')) ? get_sub_field('blog_add_content_section_tbody_tr_td_url_btn_text') : 'zum Anbieter'; ?></span>
                                </a>
                            </div>
                            <!--block 6-->
                            <div class="col-9 col-sm-8 order-4 order-sm-5 add-content__table-td-nav-tabs">
                                <?php if (get_sub_field('blog_add_content_section_tbody_tr_td_has_tabs')) : ?>
                                    <?php $hasBonus = get_sub_field('blog_add_content_section_tbody_tr_td_is_add_bonus_field'); ?>
                                    <!--Tabs-->
                                    <?php if (have_rows('blog_add_content_section_tbody_tr_td_tabs')) : ?>
                                        <ul class="nav nav-tabs <?php echo ($hasBonus) ? ' order-1 order-md-0 ' : ''; ?>"
                                            id="kpTab-<?php echo $rowCounter; ?>" role="tablist">
                                            <?php $counterTab = 1; ?>
                                            <?php while (have_rows('blog_add_content_section_tbody_tr_td_tabs')) : the_row(); ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="table-tab-<?php echo $rowCounter . '-' . $counterTab; ?>" data-toggle="tab"
                                                       href="#tab-content-<?php echo $rowCounter . '-' . $counterTab; ?>" role="tab"
                                                       aria-controls="tab-content-<?php echo $rowCounter . '-' . $counterTab; ?>" aria-selected="false"><span><?php echo get_sub_field('blog_add_content_section_tbody_tr_td_tab_title'); ?></span><span class="icon-chevron"></span></a>
                                                </li>
                                                <?php $counterTab++; ?>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <!--bonus section here-->
                                <?php if (get_sub_field('blog_add_content_section_tbody_tr_td_is_add_bonus_field')) : ?>
                                    <div class="add-content__table-td-bonus-box order-0 order-md-1">
                                        <?php
                                        $bonusAmount = get_sub_field('blog_add_content_section_tbody_tr_td_is_add_bonus_field_amount');
                                        $bonusCurr = get_sub_field('blog_add_content_section_tbody_tr_td_is_add_bonus_field_curr');
                                        $bonusDesc = get_sub_field('blog_add_content_section_tbody_tr_td_is_add_bonus_field_desc');
                                        ?>
                                        <button type="button" class="bonus-btn">
                                            <span><?php echo ($bonusAmount > 0) ? '+' . number_format($bonusAmount, '2', ',', '') : number_format($bonusAmount, '2', ',', ''); ?></span><span>&nbsp;<?php echo $bonusCurr; ?>&nbsp;Bonus</span><span class="info-icon">i</span>
                                        </button>
                                        <div class="bonus-text">
                                            <div class="bonus-text--heading"><span>Bonus</span></div>
                                            <div class="bonus-text--body"><p><?php echo $bonusDesc; ?></p></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <!--block 7-->
                            <div class="col-12 order-6 add-content__table-td-nav-content">
                                <?php if (get_sub_field('blog_add_content_section_tbody_tr_td_has_tabs')) : ?>
                                    <!--Tabs content-->
                                    <?php if (have_rows('blog_add_content_section_tbody_tr_td_tabs')) : ?>
                                    <div class="tab-content" id="kpTabContent-<?php echo $rowCounter; ?>">
                                        <?php $counterTab = 1; ?>
                                        <?php while (have_rows('blog_add_content_section_tbody_tr_td_tabs')) : the_row(); ?>
                                            <div class="tab-pane fade" id="tab-content-<?php echo $rowCounter . '-' . $counterTab; ?>" role="tabpanel"
                                             aria-labelledby="table-tab-<?php echo $rowCounter . '-' . $counterTab; ?>">
                                                <div class="container">
                                                    <?php if (have_rows('td_tab_cols')) : ?>
                                                        <div class="row">
                                                            <?php $counterColumns = 1; ?>
                                                            <?php while (have_rows('td_tab_cols')) : the_row(); ?>
                                                                <div class="col-12 col-md-6 col-lg-4">
                                                                    <?php if (have_rows('td_tab_col')) : ?>
                                                                        <?php while (have_rows('td_tab_col')) : the_row(); ?>
                                                                            <?php if (have_rows('td_tab_col_sects')) : ?>
                                                                                <?php while (have_rows('td_tab_col_sects')) : the_row(); ?>
                                                                                    <?php if (have_rows('td_tab_col_sect')) : ?>
                                                                                        <?php $counterSections = 1; ?>
                                                                                        <?php while (have_rows('td_tab_col_sect')) : the_row(); ?>
                                                                                            <!--Single column section-->
                                                                                            <div class="nav-content--section">
                                                                                                <?php
                                                                                                $tabContentSectionTitleText = get_sub_field('td_tab_col_sect_title');
                                                                                                $tabContentSectionTitleImg = get_sub_field('td_tab_col_sect_title_img');
                                                                                                ?>
                                                                                                <div class="row">
                                                                                                    <div class="col-12">
                                                                                                        <div class="title">
                                                                                                            <h4><?php echo $tabContentSectionTitleText; ?></h4>
                                                                                                            <?php if ($tabContentSectionTitleImg) : ?>
                                                                                                                <div>
                                                                                                                    <img src="<?php echo $tabContentSectionTitleImg; ?>" alt="section title image">
                                                                                                                </div>
                                                                                                            <?php endif; ?>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <?php if (have_rows('td_tab_col_sect_layout')) : ?>
                                                                                                    <?php while (have_rows('td_tab_col_sect_layout')) : the_row(); ?>
                                                                                                        <?php if( get_row_layout() == 'td_tab_col_sect_layout_stats' ) : ?>
                                                                                                            <!--Statistics content start-->
                                                                                                            <?php if (have_rows('td_tab_col_sect_layout_stats_lines')) : ?>
                                                                                                                <?php while (have_rows('td_tab_col_sect_layout_stats_lines')) : the_row();
                                                                                                                    $tabContentSectionTextBold = get_sub_field('td_tab_col_sect_layout_stats_line_t_b');
                                                                                                                    $tabContentSectionTextGreen = get_sub_field('td_tab_col_sect_layout_stats_line_t_c');
                                                                                                                    $tabContentSectionLColText = get_sub_field('td_tab_col_sect_layout_stats_line_l_text');
                                                                                                                    $tabContentSectionRColNum = get_sub_field('td_tab_col_sect_layout_stats_line_r_num');
                                                                                                                    $tabContentSectionRColText = get_sub_field('td_tab_col_sect_layout_stats_line_r_text');
                                                                                                                    ?>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-8">
                                                                                                                            <?php if ($tabContentSectionTextBold) : ?>
                                                                                                                                <strong>
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextGreen) : ?>
                                                                                                                                <span class="text-green">
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionLColText) :
                                                                                                                                echo $tabContentSectionLColText;
                                                                                                                            endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextGreen) : ?>
                                                                                                                                </span>
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextBold) : ?>
                                                                                                                                </strong>
                                                                                                                            <?php endif; ?>
                                                                                                                        </div>
                                                                                                                        <div class="col-4 text-right">
                                                                                                                            <?php if ($tabContentSectionTextBold) : ?>
                                                                                                                                <strong>
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextGreen) : ?>
                                                                                                                                <span class="text-green">
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionRColNum) :
                                                                                                                                echo number_format($tabContentSectionRColNum, '2', ',', '');
                                                                                                                            endif; ?>
                                                                                                                            <?php if ($tabContentSectionRColText) : ?>
                                                                                                                                <span>&nbsp;<?php echo $tabContentSectionRColText; ?><span>
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextGreen) : ?>
                                                                                                                                </span>
                                                                                                                            <?php endif; ?>
                                                                                                                            <?php if ($tabContentSectionTextBold) : ?>
                                                                                                                                </strong>
                                                                                                                            <?php endif; ?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                <?php endwhile; ?>
                                                                                                            <?php endif; ?>
                                                                                                            <!--Statistics content end-->
                                                                                                        <?php elseif( get_row_layout() == 'td_tab_col_sect_layout_chl' ) : ?>
                                                                                                            <!--Checklist content start-->
                                                                                                            <?php if (have_rows('td_tab_col_sect_layout_chl_lines')) : ?>
                                                                                                                <div class="row">
                                                                                                                    <div class="col-12">
                                                                                                                        <ul class="check-list">
                                                                                                                            <?php $counterDropdowns = 1; ?>
                                                                                                                            <?php while (have_rows('td_tab_col_sect_layout_chl_lines')) : the_row();
                                                                                                                                $tabContentSectionClTextBold = get_sub_field('td_tab_col_sect_layout_chl_line_t_b');
                                                                                                                                $tabContentSectionClTextGreen = get_sub_field('td_tab_col_sect_layout_chl_line_t_c');
                                                                                                                                $tabContentSectionClLColOption = get_sub_field('td_tab_col_sect_layout_chl_line_opts');
                                                                                                                                $tabContentSectionClRColText = get_sub_field('td_tab_col_sect_layout_chl_line_text');
                                                                                                                                ?>
                                                                                                                                <li class="<?php echo ($tabContentSectionClLColOption == 'yes') ? 'check' : 'times'; ?>">
                                                                                                                                    <?php if (get_sub_field('td_tab_col_sect_layout_chl_line_has_dd')) : ?>
                                                                                                                                        <a class="nav-content--section-dropdown-btn"
                                                                                                                                           data-toggle="collapse"
                                                                                                                                           href="#dropdown-line-<?php echo $rowCounter .'-'. $counterColumns .'-'. $counterSections .'-'. $counterDropdowns; ?>"
                                                                                                                                           role="button"
                                                                                                                                           aria-expanded="false"
                                                                                                                                           aria-controls="dropdown-line-<?php echo $rowCounter .'-'. $counterColumns .'-'. $counterSections .'-'. $counterDropdowns; ?>">
                                                                                                                                            <?php if ($tabContentSectionClTextBold) : ?>
                                                                                                                                                <strong>
                                                                                                                                            <?php endif; ?>
                                                                                                                                            <?php if ($tabContentSectionClTextGreen) : ?>
                                                                                                                                                <span class="text-green">
                                                                                                                                            <?php endif; ?>
                                                                                                                                            <?php if ($tabContentSectionClRColText) : ?>
                                                                                                                                                <span><?php echo $tabContentSectionClRColText; ?></span><span class="icon-chevron"></span>
                                                                                                                                            <?php endif; ?>
                                                                                                                                            <?php if ($tabContentSectionClTextGreen) : ?>
                                                                                                                                                </span>
                                                                                                                                            <?php endif; ?>
                                                                                                                                            <?php if ($tabContentSectionClTextBold) : ?>
                                                                                                                                                </strong>
                                                                                                                                            <?php endif; ?>
                                                                                                                                        </a>
                                                                                                                                        <div class="collapse" id="dropdown-line-<?php echo $rowCounter .'-'. $counterColumns .'-'. $counterSections .'-'. $counterDropdowns; ?>">
                                                                                                                                            <?php
                                                                                                                                            $dropdownLineLCol = get_sub_field('td_tab_col_sect_layout_chl_line_dd_l_col_t');
                                                                                                                                            $dropdownLineRColNum = get_sub_field('td_tab_col_sect_layout_chl_line_dd_r_col_n');
                                                                                                                                            $dropdownLineRColText = get_sub_field('td_tab_col_sect_layout_chl_line_dd_r_col_t');
                                                                                                                                            ?>
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-8">
                                                                                                                                                    <?php if ($dropdownLineLCol) : ?>
                                                                                                                                                        <span><?php echo $dropdownLineLCol; ?></span>
                                                                                                                                                    <?php endif; ?>
                                                                                                                                                </div>
                                                                                                                                                <div class="col-4 text-right">
                                                                                                                                                    <?php if ($dropdownLineRColNum) : ?>
                                                                                                                                                        <span><?php echo number_format($dropdownLineRColNum, '2', ',', ''); ?></span>
                                                                                                                                                    <?php endif; ?>
                                                                                                                                                    <?php if ($dropdownLineRColText) : ?>
                                                                                                                                                        <span>&nbsp;<?php echo $dropdownLineRColText; ?></span>
                                                                                                                                                    <?php endif; ?>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    <?php else : ?>
                                                                                                                                        <?php if ($tabContentSectionClTextBold) : ?>
                                                                                                                                            <strong>
                                                                                                                                        <?php endif; ?>
                                                                                                                                        <?php if ($tabContentSectionClTextGreen) : ?>
                                                                                                                                            <span class="text-green">
                                                                                                                                        <?php endif; ?>
                                                                                                                                        <?php if ($tabContentSectionClRColText) : ?>
                                                                                                                                            <span><?php echo $tabContentSectionClRColText; ?></span>
                                                                                                                                        <?php endif; ?>
                                                                                                                                        <?php if ($tabContentSectionClTextGreen) : ?>
                                                                                                                                            </span>
                                                                                                                                        <?php endif; ?>
                                                                                                                                        <?php if ($tabContentSectionClTextBold) : ?>
                                                                                                                                            </strong>
                                                                                                                                        <?php endif; ?>
                                                                                                                                    <?php endif; ?>
                                                                                                                                </li>
                                                                                                                                <?php $counterDropdowns++; ?>
                                                                                                                            <?php endwhile; ?>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <?php endif; ?>
                                                                                                            <!--Checklist content end-->
                                                                                                        <?php endif; ?>
                                                                                                    <?php endwhile; ?>
                                                                                                <?php endif; ?>
                                                                                            </div>
                                                                                            <?php $counterSections++; ?>
                                                                                        <?php endwhile; ?>
                                                                                    <?php endif; ?>
                                                                                <?php endwhile; ?>
                                                                            <?php endif; ?>
                                                                        <?php endwhile; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <?php $counterColumns++; ?>
                                                            <?php endwhile; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <?php $counterTab++; ?>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php $rowCounter++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            <!--Table body end-->
        </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'The table has not been found', 'astra-child' ); ?></p>
<?php endif; ?>
