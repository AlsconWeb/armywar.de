<?php
/**
 * Template part for displaying single blog posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

?>

<?php astra_entry_before(); ?>

<article
    <?php
    echo astra_attr(
        'article-single',
        array(
            'itemtype'  => 'https://schema.org/CreativeWork',
            'itemscope' => 'itemscope',
            'id'        => 'post-' . get_the_id(),
            'class'     => join( ' ', get_post_class() ),
        )
    );
    ?>
>

    <?php astra_entry_top(); ?>

    <?php /* astra_entry_content_single(); */ ?>
    <div <?php astra_blog_layout_class( 'single-layout-1' ); ?>>

        <?php astra_single_header_before(); ?>

        <header class="entry-header <?php astra_entry_header_class(); ?>">

            <?php astra_single_header_top(); ?>

            <?php /* astra_blog_post_thumbnail_and_title_order(); */
            astra_get_single_post_title_meta();
            ?>

            <div class="custom-post-meta">
                <div class="custom-post-meta--avatar">
                    <?php echo get_avatar( get_the_author_meta('user_email'), 40 ); ?>
                </div>
                <div class="custom-post-meta--text">
                    <div class="custom-post-meta--author">
                        <span><?php echo __('Von', 'astra-child') . ' ' . astra_post_author(); ?></span>
                    </div>
                    <div class="custom-post-meta--date">
                        <span><?php echo __('Letztes Update', 'astra-child') . ': ' . esc_html( get_the_modified_date( 'd.m.Y' ) ); ?></span>
                    </div>
                </div>
            </div>

            <?php astra_single_header_bottom(); ?>

        </header><!-- .entry-header -->

        <?php astra_single_header_after(); ?>

        <div class="entry-content clear" itemprop="text">

            <?php astra_entry_content_before(); ?>

            <?php astra_get_post_thumbnail( '<div class="ast-custom-right-post-thumb">', '</div>' ); ?>

            <?php the_content(); ?>

            <?php if( get_field('blog_is_add_sections') ) : ?>
                <?php if( have_rows('blog_add_content_flex_sections') ) :
                    $contentsTitle = get_field('blog_table_of_contents');
                    ?>
                    <div class="add-table-contents">
                        <h4 class="add-table-contents__title"><?php echo $contentsTitle ?></h4>
                        <ol class="add-table-contents__list">
                            <?php while( have_rows('blog_add_content_flex_sections') ) : the_row(); ?>
                                <?php
                                $counter = 1;
                                $sectionID = str_replace( " ", "-", trim( get_sub_field('blog_add_content_section_id') ) );
                                $sectionTitle = get_sub_field('blog_add_content_section_title');
                                ?>
                                <li><a href="#<?php echo $sectionID . '-' . $counter; ?>"><? echo esc_html($sectionTitle); ?></a></li>
                                <?php $counter++; ?>
                            <?php endwhile; ?>
                        </ol>
                    </div>
                    <?php while( have_rows('blog_add_content_flex_sections') ) : the_row(); ?>
                    <?php
                    $counter = 1;
                    $sectionTitle = get_sub_field('blog_add_content_section_title');
                    $sectionID = str_replace( " ", "-", trim( get_sub_field('blog_add_content_section_id') ) );
                    ?>
                    <div class="add-content-section">
                        <h3 class="add-content-section__title" id="<?php echo $sectionID . '-' . $counter; ?>"><? echo esc_html($sectionTitle); ?></h3>

                        <?php if(have_rows('blog_add_section_contents')):
                            while(have_rows('blog_add_section_contents')): the_row();
                                if( have_rows('blog_add_section_content_types') ):
                                    while ( have_rows('blog_add_section_content_types') ): the_row();
                                        if( get_row_layout() == 'text_section' ):
                                            $textContent = get_sub_field('text_section_content');
                                            echo '<div class="add-content-section__content">' . $textContent . '</div>';

                                        elseif( get_row_layout() == 'table_section' ):
                                            get_template_part('template-parts/content', 'tables');

                                        elseif( get_row_layout() == 'product_section' ):
                                            get_template_part('template-parts/content', 'products');

                                        elseif( get_row_layout() == 'pros_cons_section' ):
                                            get_template_part('template-parts/content', 'prosncons');

                                        endif;
                                    endwhile;
                                endif;
                            endwhile;
                        endif; ?>

                    </div>
                    <?php $counter++; ?>
                <?php endwhile; ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            astra_edit_post_link(

                sprintf(
                /* translators: %s: Name of current post */
                    esc_html__( 'Edit %s', 'astra' ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>

            <?php astra_entry_content_after(); ?>

            <?php
            wp_link_pages(
                array(
                    'before'      => '<div class="page-links">' . esc_html( astra_default_strings( 'string-single-page-links-before', false ) ),
                    'after'       => '</div>',
                    'link_before' => '<span class="page-link">',
                    'link_after'  => '</span>',
                )
            );
            ?>
        </div><!-- .entry-content .clear -->
    </div>

    <?php astra_entry_bottom(); ?>

</article><!-- #post-## -->

<?php astra_entry_after(); ?>
