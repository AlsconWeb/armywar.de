jQuery(document).ready(function ($) {
    /*Table of contents in the sidebar*/
    function Utils() {

    }

    Utils.prototype = {
        constructor: Utils,
        isElementInView: function (element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offset().top;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
        }
    };

    var Utils = new Utils();

    $(window).scroll(function() {
        var isElementInView = Utils.isElementInView($('.site-main .add-table-contents'), false);
        var isThumbnailView = Utils.isElementInView($('.site-main .ast-custom-right-post-thumb'), false);

        if ((isElementInView) || (!isElementInView && isThumbnailView)) {
            //console.log('in view');
            if($('.secondary .sidebar-main .add-table-contents').length !== 0) {
                $('.secondary .sidebar-main .add-table-contents').remove();
            }
        } else {
            //console.log('out of view');
            if($('.secondary .sidebar-main .add-table-contents').length === 0) {
                $('.site-main .add-table-contents').clone().appendTo('.secondary .sidebar-main');
            }
            if($('.secondary .sidebar-main .add-table-contents').length !== 0) {
                var scrolled = $(window).scrollTop();
                $('.secondary .sidebar-main .add-table-contents').css({
                    'position': 'sticky',
                    'left': 0,
                    'top': '100px',
                });
            }
        }
    });

    /**/

    /* toggle automaten info popup */
    $('.info-btn-automaten').click(function() {
        $('.automaten-block').removeClass('open');
        $(this).siblings('.automaten-block').addClass('open');
    });

    /* toggle bonus info popup */
    $('.bonus-btn').click(function() {
        $('.bonus-text').removeClass('open');
        $(this).siblings('.bonus-text').addClass('open');
    });

    /* toggle table heading info popup */
    $('.info-btn').click(function() {
        $('.info-text').removeClass('open');
        $(this).siblings('.info-text').addClass('open');
    });

    $(document).click(function (e) {
        // if the target of the click isn't the container nor a descendant of the container
        // close automaten info block on aside click
        var infoABtn = $('.info-btn-automaten'),
            infoABox = $('.automaten-block');

        if (!infoABtn.is(e.target) && infoABtn.has(e.target).length === 0 && !infoABox.is(e.target) && infoABox.has(e.target).length === 0) {
            $('.automaten-block').removeClass('open');
        }

        // close bonus info block on aside click
        var infoBonusBtn = $('.bonus-btn'),
            infoBonusBox = $('.bonus-text');

        if (!infoBonusBtn.is(e.target) && infoBonusBtn.has(e.target).length === 0 && !infoBonusBox.is(e.target) && infoBonusBox.has(e.target).length === 0) {
            $('.bonus-text').removeClass('open');
        }

        // close table heading info block on aside click
        var infoBtn = $('.info-btn'),
            infoBox = $('.info-text');

        if (!infoBtn.is(e.target) && infoBtn.has(e.target).length === 0 && !infoBox.is(e.target) && infoBox.has(e.target).length === 0) {
            $('.info-text').removeClass('open');
        }
    });

    /* hide and show bootstrap nav tabs content (additional functionality) */
    $('.add-content__table-td-nav-tabs .nav-tabs .nav-link').click(function(e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active show').attr('aria-selected', 'false');
            $('.add-content__table-td-nav-content .tab-content .tab-pane').removeClass('active show');
            e.stopPropagation();
        }
    });
});
