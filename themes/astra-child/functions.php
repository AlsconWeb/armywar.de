<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );
define( 'ASTRA_THEME_DIR', trailingslashit( get_template_directory() ) );
require_once ASTRA_THEME_DIR . 'inc/class-astra-loop.php';

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
    wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
    wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/libs/bootstrap/js/bootstrap.bundle.min.js', array(), '4.0.0', true );
    wp_enqueue_script( 'astra-child-theme-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

remove_action('astra_template_parts_content', array(Astra_Loop::get_instance(), 'template_parts_post'));
class Astra_Loop_Child extends Astra_Loop {
    /**
     * Template part single
     *
     * @since 1.2.7
     * @return void
     */
    public function __construct() {
        add_action( 'astra_template_parts_content', array( $this, 'blog_template_parts_post' ) );
    }

    public function blog_template_parts_post() {
        if ( is_single() && in_category('blog') ) {
            get_template_part('template-parts/content', 'single-blog');
            return;
        } else if ( is_single() && !in_category('blog') ) {
            get_template_part('template-parts/content', 'single');
        }
    }
}
new Astra_Loop_Child;

/**
 * Tables post type
 */
function tables_post_type() {

    // Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Tables', 'Post Type General Name', 'astra-child'),
        'singular_name' => _x('Table', 'Post Type Singular Name', 'astra-child'),
        'menu_name' => __('Tables', 'astra-child'),
        'name_admin_bar' => __( 'Table', 'astra-child' ),
        'archives' => __( 'Table Archives', 'astra-child' ),
        'attributes' => __( 'Table Attributes', 'astra-child' ),
        'parent_item_colon' => __('Parent Table', 'astra-child'),
        'all_items' => __('All Tables', 'astra-child'),
        'view_item' => __('View Table', 'astra-child'),
        'view_items' => __( 'View Tables', 'astra-child' ),
        'add_new_item' => __('Add New Table', 'astra-child'),
        'add_new' => __('Add New', 'astra-child'),
        'new_item' => __( 'New Table', 'astra-child' ),
        'edit_item' => __('Edit Table', 'astra-child'),
        'update_item' => __('Update Table', 'astra-child'),
        'search_items' => __('Search Tables', 'astra-child'),
        'not_found' => __('Not Found', 'astra-child'),
        'not_found_in_trash' => __('Not found in Trash', 'astra-child'),
        'featured_image' => __( 'Featured Image', 'astra-child' ),
        'set_featured_image' => __( 'Set featured image', 'astra-child' ),
        'remove_featured_image' => __( 'Remove featured image', 'astra-child' ),
        'use_featured_image' => __( 'Use as featured image', 'astra-child' ),
        'insert_into_item' => __( 'Paste into table', 'astra-child' ),
        'uploaded_to_this_item' => __( 'Uploaded to this table', 'astra-child' ),
        'items_list' => __( 'Tables list', 'astra-child' ),
        'items_list_navigation' => __( 'Tables list navigation', 'astra-child' ),
        'filter_items_list' => __( 'Filter tables list', 'astra-child' ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label' => __( 'Table', 'astra-child' ),
        'description' => __('Table description', 'astra-child'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'custom-fields',), /* 'excerpt', 'author', 'thumbnail', 'comments', 'revisions',  */
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('sections', 'table_category',), /* 'post_tag' */
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_icon'   => 'dashicons-editor-table',
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'tables'),
    );

    // Registering your Custom Post Type
    register_post_type('tables', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/
add_action('init', 'tables_post_type', 0);

/**
 * Products post type
 */
function products_post_type() {

    // Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Products', 'Post Type General Name', 'astra-child'),
        'singular_name' => _x('Product', 'Post Type Singular Name', 'astra-child'),
        'menu_name' => __('Products', 'astra-child'),
        'name_admin_bar' => __( 'Product', 'astra-child' ),
        'archives' => __( 'Product Archives', 'astra-child' ),
        'attributes' => __( 'Product Attributes', 'astra-child' ),
        'parent_item_colon' => __('Parent Product', 'astra-child'),
        'all_items' => __('All Products', 'astra-child'),
        'view_item' => __('View Product', 'astra-child'),
        'view_items' => __( 'View Products', 'astra-child' ),
        'add_new_item' => __('Add New Product', 'astra-child'),
        'add_new' => __('Add New', 'astra-child'),
        'new_item' => __( 'New Product', 'astra-child' ),
        'edit_item' => __('Edit Product', 'astra-child'),
        'update_item' => __('Update Product', 'astra-child'),
        'search_items' => __('Search Products', 'astra-child'),
        'not_found' => __('Not Found', 'astra-child'),
        'not_found_in_trash' => __('Not found in Trash', 'astra-child'),
        'featured_image' => __( 'Featured Image', 'astra-child' ),
        'set_featured_image' => __( 'Set featured image', 'astra-child' ),
        'remove_featured_image' => __( 'Remove featured image', 'astra-child' ),
        'use_featured_image' => __( 'Use as featured image', 'astra-child' ),
        'insert_into_item' => __( 'Paste into product', 'astra-child' ),
        'uploaded_to_this_item' => __( 'Uploaded to this product', 'astra-child' ),
        'items_list' => __( 'Products list', 'astra-child' ),
        'items_list_navigation' => __( 'Products list navigation', 'astra-child' ),
        'filter_items_list' => __( 'Filter products list', 'astra-child' ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label' => __( 'Product', 'astra-child' ),
        'description' => __('Product description', 'astra-child'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'custom-fields',), /* 'excerpt', 'author', 'thumbnail', 'comments', 'revisions',  */
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('sections', 'product_category',), /* 'post_tag' */
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_icon'   => 'dashicons-cart',
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'products'),
    );

    // Registering your Custom Post Type
    register_post_type('products', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/
add_action('init', 'products_post_type', 0);

//Limit the excerpt by the amount characters
function the_excerpt_max_charlength( $charlength ){
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            return mb_substr( $subex, 0, $excut ) . '...';
        } else {
            return $subex . '...';
        }
    } else {
        return $excerpt;
    }
}

//Shortcode for the product block
add_shortcode( 'meinprodukt', 'meinprodukt_shortcode' );

function meinprodukt_shortcode( $atts ){

    $atts = shortcode_atts( [
        'number' => null
    ], $atts );

    //args
    $products_args = array(
        'post_type'		    => 'products',
        'posts_per_page'	=> 1,
        'meta_key'		    => 'blog_add_content_section_prod_id',
        'meta_value'	    => $atts['number']
    );

    // query
    $products = new WP_Query( $products_args );
    $out = '';

    if( $products->have_posts() ):
        while( $products->have_posts() ) : $products->the_post();
        $out .= '
        <!--Product section start-->
            <div class="container px-0 my-3">
                <div class="add-content__product">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-3">
                            <div class="add-content__product-img text-center">
                                <a href="' . get_field("blog_add_content_section_prod_url") . '" target="_blank" title="' . strip_tags( get_the_content() ) . '">
                                    <img src="' . get_field("blog_add_content_section_prod_img") . '" alt="product image" >
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="add-content__product-content">
                                <div class="add-content__product-content-desc">
                                    <a href="' . get_field("blog_add_content_section_prod_url") . '" target="_blank" title="' . strip_tags( get_the_content() ) . '">' . the_excerpt_max_charlength(100) . '</a>
                                </div>';
                                if (have_rows('blog_add_content_section_prod_note')) :
                                    while (have_rows('blog_add_content_section_prod_note')) : the_row();
                                        $noteText = get_sub_field('blog_add_content_section_prod_note_t');
                                        $noteNum = get_sub_field('blog_add_content_section_prod_note_n');
                                        if ($noteText || $noteNum) :
                                            $out .= '<div class="add-content__product-content-note">
                                                <span>' . __("Gesamtnote", "astra-child") . ':&nbsp;</span>';
                                                if ($noteText) :
                                                    $out .= '<span class="add-content__product-content-note-text">' . $noteText . '</span>';
                                                endif;
                                                if ($noteNum) :
                                                    $out .= '<span class="add-content__product-content-note-num">&nbsp;(' . $noteNum . ')</span>';
                                                endif;
                                            $out .= '</div>';
                                        endif;
                                    endwhile;
                                endif;
                $out .= '</div>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="' . get_field("blog_add_content_section_prod_url") . '" role="button" target="_blank" class="add-content__product-btn">
                                <span class="icon-cart"></span>';
                                $marketPlace = get_field('blog_add_content_section_prod_marketplace');
                                if ($marketPlace) :
                                    $out .= '<span>' . $marketPlace . '&nbsp;</span>';
                                endif;
                                if (have_rows('blog_add_content_section_prod_price')) :
                                    while (have_rows('blog_add_content_section_prod_price')) : the_row();
                                        $priceNum = get_sub_field('blog_add_content_section_prod_price_amount');
                                        $priceCurr = get_sub_field('blog_add_content_section_prod_price_currency');
                                        if ($priceCurr) :
                                            $out .= '<span>' . $priceCurr . '&nbsp;</span>';
                                        endif;
                                        if ($priceNum) :
                                            $out .= '<span>' . $priceNum . '</span>';
                                        endif;
                                    endwhile;
                                endif;
                $out .= '</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--Product section end-->';
        endwhile;
        wp_reset_postdata();
    else :
        $out .= '<p>' . __( "The product has not been found", "astra-child" ) . '</p>';
    endif;

    return $out;
}
