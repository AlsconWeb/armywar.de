<?php
/**
 * 
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * 
 * 
 * 
 *
 * @wordpress-plugin
 * Plugin Name:       Fake browser history
 * Description:       Redirect visitors when they click the back button.
 * Version:           1.0.0
 * Author:            Paul B
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       fakebrowserhistory

 */

    add_action('init', function(){
    include dirname(__FILE__) .'/FakeBrowserHistoryMetabox.php';
    include dirname(__FILE__) .'/FakeBrowserHistorySettings.php';
    add_action('wp_head', 'FakeBrowserHistory');
    add_action( 'admin_menu', 'fake_browser_history_add_admin_menu' );
    add_action( 'admin_init', 'fake_browser_history_settings_init' );  
});

function FakeBrowserHistory(){

  if (  is_page() || is_single() ) {
  
    if(  get_post_meta(get_the_ID(), 'activate_browser_back_redirect_activate_browser_back_redirect', true ) !== 'activate-browser-back-redirect'){
      echo "<script>";
       ?>

     
     <?php 
      $options = get_option( 'fake_browser_history_settings' );

      $redirecturls = $options['fake_browser_history_text_field_1'];
        if(isset($redirecturls)) {
          ?>

    function AddRedirectHook(theRules) { 

       var theKey = 'default'; 
       var theReferrer = document.referrer;    
       for (var key in theRules) { 

 
         if (theReferrer.indexOf(key) > -1) { 
       
             theKey = key; 
            
             break; 
         } 
       }  
      var RedirectOnce = sessionStorage.getItem('redirected'); 
      if(typeof theRules['default'] == "undefined" && theKey== 'default'){
    }
       else{

        if ( RedirectOnce == null) { 
    
       var b = location.pathname;
      var x = location.hash;
       var c = b.concat(x);
              history.replaceState(null, null,c); 
            history.pushState(null, null, c);
            sessionStorage.setItem('redirected', true); 
       }
     }

     
     window.addEventListener("popstate", function() { 
         if (location.hash == x && theKey != null) { 
              history.pushState(null, null, location.pathname); 
             history.replaceState(null, null, location.pathname); 
             setTimeout(function() { 
               sessionStorage.clear();  
                 location.replace(theRules[theKey]); 
             }, 0); 
         } 
       }); 
          }
          var theRules = {
          <?php
          $d= 0;
            foreach($redirecturls as $redirecturl){
              if(isset($redirecturl['name']) && strlen($redirecturl['url']) > 1){
                $d++;
                if(strpos($redirecturl['url'],'<TITLE>') !== FALSE ){
                  echo " '".$redirecturl['name']."' : ' ".str_replace(' ', '+',str_replace('<TITLE>', get_the_title(), $redirecturl['url']))."',";

                }else{
 echo " '".$redirecturl['name']."' : ' ".$redirecturl['url']."',";

                }
              }
            } 
            ?>
          }
          <?php
            if($d>0){
              echo " jQuery( document ).ready(function() {
                AddRedirectHook(theRules)
              });";
            }
           ?>
       
        <?php }?>
       



      <?php 
      echo "</script>";
      } 
  }
}





?>
