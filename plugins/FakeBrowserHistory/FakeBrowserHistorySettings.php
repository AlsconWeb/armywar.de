<?php
function fake_browser_history_add_admin_menu(  ) { 

	add_menu_page( ' Fake browser history', ' Fake browser history', 'manage_options', '_fake_browser_history', 'fake_browser_history_options_page' );

}


function fake_browser_history_settings_init(  ) { 

	register_setting( 'fakebrowser', 'fake_browser_history_settings' );

	add_settings_section(
		'fake_browser_history_pluginPage_section', 
		__( 'Change your redirect urls here', 'fakebrowserhistory' ), 
		'fake_browser_history_settings_section_callback', 
		'fakebrowser'
	);

	add_settings_field( 
		'fake_browser_history_text_field_0', 
		__( 'Default', 'fakebrowserhistory' ), 
		'fake_browser_history_text_field_0_render', 
		'fakebrowser', 
		'fake_browser_history_pluginPage_section' 
	);
	add_settings_field( 
		'fake_browser_history_text_field_1', 
		__( 'Google', 'fakebrowserhistory' ), 
		'fake_browser_history_text_field_1_render', 
		'fakebrowser', 
		'fake_browser_history_pluginPage_section' 
	);

	add_settings_field( 
		'fake_browser_history_text_field_2', 
		__( 'Yahoo', 'fakebrowserhistory' ), 
		'fake_browser_history_text_field_2_render', 
		'fakebrowser', 
		'fake_browser_history_pluginPage_section' 
	);

	add_settings_field( 
		'fake_browser_history_text_field_3', 
		__( 'Bing', 'fakebrowserhistory' ), 
		'fake_browser_history_text_field_3_render', 
		'fakebrowser', 
		'fake_browser_history_pluginPage_section' 
	);



}


function fake_browser_history_text_field_0_render(  ) { 

	$options = get_option( 'fake_browser_history_settings' );

	?>
  <input style='display:none!important' type='text' name='fake_browser_history_settings[fake_browser_history_text_field_1][0][name]' value='default'>
	<input type='text' size="160" name='fake_browser_history_settings[fake_browser_history_text_field_1][0][url]' 
         value='<?php echo $options['fake_browser_history_text_field_1'][0]['url']; ?>'>
	<?php

}


function fake_browser_history_text_field_1_render(  ) { 

	$options = get_option( 'fake_browser_history_settings' );
	?>
  <input style='display:none!important' type='text' name='fake_browser_history_settings[fake_browser_history_text_field_1][1][name]' value='google'>
	<input type='text' size="160" name='fake_browser_history_settings[fake_browser_history_text_field_1][1][url]' value='<?php echo $options['fake_browser_history_text_field_1'][1]['url']; ?>'>
	<?php

}


function fake_browser_history_text_field_2_render(  ) { 

  
	$options = get_option( 'fake_browser_history_settings' );
 
	?>
  <input style='display:none!important' type='text' name='fake_browser_history_settings[fake_browser_history_text_field_1][2][name]' value='Yahoo'>
	<input type='text' size="160" name='fake_browser_history_settings[fake_browser_history_text_field_1][2][url]' value='<?php echo $options['fake_browser_history_text_field_1'][2]['url']; ?>'>
	<?php

}


function fake_browser_history_text_field_3_render(  ) { 

	$options = get_option( 'fake_browser_history_settings' );
	?>
  <input style='display:none!important' type='text' name='fake_browser_history_settings[fake_browser_history_text_field_1][3][name]' value='Bing'>
	<input type='text' size="160" name='fake_browser_history_settings[fake_browser_history_text_field_1][3][url]' value='<?php echo $options['fake_browser_history_text_field_1'][3]['url']; ?>'>
	<?php

}


function fake_browser_history_settings_section_callback(  ) { 

	echo __( 'Add full urls to redirect too, for example "https://www.urltoredirecttoo.de/page. Add &lt;TITLE&gt; to include the page title in redirect" ', 'fakebrowserhistory' );

}


function fake_browser_history_options_page(  ) { 

	?>
	<form action='options.php' method='post'>

		<h2> Fake browser history</h2>

		<?php
		settings_fields( 'fakebrowser' );
		do_settings_sections( 'fakebrowser' );
		submit_button();
		?>

	</form>
	<?php

}
