<?php 
function activate_browser_back_redirect_get_meta( $value ) {
	global $post;

	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}

function activate_browser_back_redirect_add_meta_box() {
	add_meta_box(
		'activate_browser_back_redirect-activate-browser-back-redirect',
		__( 'Deactivate Browser back redirect', 'activate_browser_back_redirect' ),
		'activate_browser_back_redirect_html',
		'post',
		'normal',
		'default'
	);
	add_meta_box(
		'activate_browser_back_redirect-activate-browser-back-redirect',
		__( 'Deactivate Browser back redirect', 'activate_browser_back_redirect' ),
		'activate_browser_back_redirect_html',
		'page',
		'normal',
		'default'
	);
}
add_action( 'add_meta_boxes', 'activate_browser_back_redirect_add_meta_box' );

function activate_browser_back_redirect_html( $post) {
	wp_nonce_field( '_activate_browser_back_redirect_nonce', 'activate_browser_back_redirect_nonce' ); ?>

	<p>Check the box to disablexx the redirect</p>

	<p>

		<input type="checkbox" name="activate_browser_back_redirect_activate_browser_back_redirect" id="activate_browser_back_redirect_activate_browser_back_redirect" value="activate-browser-back-redirect" <?php echo ( activate_browser_back_redirect_get_meta( 'activate_browser_back_redirect_activate_browser_back_redirect' ) === 'activate-browser-back-redirect' ) ? 'checked' : ''; ?>>
		<label for="activate_browser_back_redirect_activate_browser_back_redirect"><?php _e( 'Disable Browser back redirect', 'activate_browser_back_redirect' ); ?></label>	</p><?php
}

function activate_browser_back_redirect_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['activate_browser_back_redirect_nonce'] ) || ! wp_verify_nonce( $_POST['activate_browser_back_redirect_nonce'], '_activate_browser_back_redirect_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['activate_browser_back_redirect_activate_browser_back_redirect'] ) )
		update_post_meta( $post_id, 'activate_browser_back_redirect_activate_browser_back_redirect', esc_attr( $_POST['activate_browser_back_redirect_activate_browser_back_redirect'] ) );
	else
		update_post_meta( $post_id, 'activate_browser_back_redirect_activate_browser_back_redirect', null );
}
add_action( 'save_post', 'activate_browser_back_redirect_save' );

/*
	Usage: activate_browser_back_redirect_get_meta( 'activate_browser_back_redirect_activate_browser_back_redirect' )
*/
?>